import { Decorator } from "./decorator";
import { BTBoard, BTStatus } from "../node";


type RepeaterProps = {
    times: number
};

export class Repeater extends Decorator<RepeaterProps> {
    private counter = 0;
    public pulse(board: BTBoard, status: BTStatus): void {
        if (status == BTStatus.Success && this.status == status) {
            return;
        }
        if (status == BTStatus.Start) {
            this.status = BTStatus.Running;
            this.counter = 0;
        }
        if (this.counter >= this.props.times) {
            this.status = BTStatus.Success;
            board.root.notify(this.parent, this.status);
            // board.root.notify(this, this.status);
        } else {
            this.counter++;
            board.root.notify(this.child, BTStatus.Start);
        }
    }
}