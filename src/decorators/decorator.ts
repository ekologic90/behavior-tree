import { Node } from "../node";
import { Composite } from '../composites/composite';

export abstract class Decorator<Prop> extends Composite<Prop> {
  protected get child() {
    return this.children[0];
  }

  constructor(props) {
    super(props);
    this.children.push(null);
  }

  public addChild(node: Node): void {
    this.children[0] = node;
  }
}