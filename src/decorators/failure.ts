import { Decorator } from "./decorator";
import { BTBoard, BTStatus } from "../node";


type FailureProps = {
};

export class Failure extends Decorator<FailureProps> {
    public pulse(board: BTBoard, status: BTStatus): void {
        if (status == BTStatus.Start) {
            this.status = BTStatus.Running;
            this.child.pulse(board, BTStatus.Start);
        }
        else {
            this.status = BTStatus.Success;
            board.root.notify(this.parent, BTStatus.Failure);
        }
    }
}