import { Decorator } from "./decorator";
import { BTBoard, BTStatus } from "../node";

type InverterProps = {

};

export class Inverter extends Decorator<InverterProps> {
  public pulse(board: BTBoard, status: BTStatus): void {
    if (status == BTStatus.Start) {
      this.child.pulse(board, BTStatus.Start);
    }
    if (status == BTStatus.Failure) {
      board.root.notify(this._parent, BTStatus.Success);
    }
    else if (status == BTStatus.Success) {
      board.root.notify(this._parent, BTStatus.Failure);
    }
  }
}