import { Decorator } from "./decorator";
import { BTBoard, BTStatus } from "../node";


type RepeatUntilProps = {
    predicate: () => boolean;
}

export class RepeatUntil extends Decorator<RepeatUntilProps> {

    public pulse(board: BTBoard, status: BTStatus): void {
        
    }
}