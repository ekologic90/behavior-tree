import { BehaviorTree } from '../behavior-tree';
import { Node, BTBoard, BTStatus } from '../node';
import { Leaf } from '../leafs/leaf'
import { Action } from '../leafs/action';
import { Condition } from '../leafs/condition';
import { Sequencer } from '../composites/sequencer';

type ConditionalActionProps = {
  predicate: () => boolean,
  action: (done: () => void) => void,
  async?: boolean,
}

export class ConditionalAction extends Leaf<ConditionalActionProps> {
  private child: Node;

  constructor(props) {
    super(props);

    this.child = this.getChildren(props);
    this.child.setParent(this);
  }

  public start(board: BTBoard) {
    if (this.status == BTStatus.Running) {
      return;
    }

    this.status = BTStatus.Running;
    board.root.notify(this.child, BTStatus.Start);
  }

  public success(board: BTBoard) {
    this.status = BTStatus.Success;
    board.root.notify(this.parent, this.status);
  }

  public cancel(board: BTBoard) {
    this.status = BTStatus.Cancel;
    board.root.notify(this.parent, this.status);
  }

  public failure(board: BTBoard) {
    this.status = BTStatus.Failure;
    // Wrong should not notify parent as success
    board.root.notify(this.parent,BTStatus.Success);
  }

  private getChildren(props: ConditionalActionProps) {
    return (
      <Sequencer>
        <Condition {...props} />
        <Action {...props} />
      </Sequencer>
    )
  }
}