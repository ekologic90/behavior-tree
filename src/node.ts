import { BehaviorTree } from "./behavior-tree";

export type BTBoard = {
  root: BehaviorTree;
}

export enum BTStatus {
  Idle,
  Start,
  Running,
  Success,
  Failure,
  Cancel
};

let id = 0;

export abstract class Node<Prop = {}> implements JSX.ElementClass {
  private _id: string;
  public get id() { return this._id; }
  private pulseFunctions: Map<BTStatus, (board: BTBoard) => void> = new Map();
  public status: BTStatus = BTStatus.Idle;
  protected _parent: Node;
  protected props: Prop;
  public get parent() { return this._parent; }
  constructor(props: Prop) {
    this.props = props;
    this._id = this.constructor.name + (id++).toString();
    this.pulseFunctions.set(BTStatus.Idle, this.idle.bind(this));
    this.pulseFunctions.set(BTStatus.Start, this.start.bind(this));
    this.pulseFunctions.set(BTStatus.Running, this.running.bind(this));
    this.pulseFunctions.set(BTStatus.Success, this.success.bind(this));
    this.pulseFunctions.set(BTStatus.Failure, this.failure.bind(this));
    this.pulseFunctions.set(BTStatus.Cancel, this.cancel.bind(this));
  }
  public setParent(parent: Node): void {
    this._parent = parent;
  }
  public pulse(board: BTBoard, status: BTStatus): void {
    if (status != this.status) {
      this.pulseFunctions.get(status)(board);
    }
  }
  public idle(board: BTBoard): void {/* no-op */ }
  public start(board: BTBoard): void {/* no-op */ }
  public running(board: BTBoard): void {/* no-op */ }
  public success(board: BTBoard): void {/* no-op */ }
  public failure(board: BTBoard): void {/* no-op */ }
  public cancel(board: BTBoard): void {/* no-op */ }
}

Object.defineProperty(Node, 'parent', {
  enumerable: false,
})
