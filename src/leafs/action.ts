import { Leaf } from './leaf';
import { BTBoard, BTStatus } from '../node';

type ActionProp = {
  action: (done?: () => void) => void,
  async?: boolean
}

export class Action extends Leaf<ActionProp>{
  public idle(board: BTBoard) {
    if (this.status == BTStatus.Running) {
      return;
    }

    this.status = BTStatus.Idle;
    board.root.notify(this, this.status);
  }

  public start(board: BTBoard) {
    if (this.status == BTStatus.Running) {
      return;
    }
    
    this.status = BTStatus.Running;
    if (this.props.async) {
      board.root.notify(this, this.status);
      this.props.action(() => this.handleSuccess(board));
    } else {
      this.props.action();
      this.handleSuccess(board);
    }
  }

  private handleSuccess(board: BTBoard) {
    this.status = BTStatus.Success;
    board.root.notify(this.parent, BTStatus.Success);
    board.root.notify(this, BTStatus.Success);
  }
}