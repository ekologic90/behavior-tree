import { BehaviorTree } from '../../behavior-tree';
import { Condition } from '../condition';
import { BTStatus } from '../../node';

jest.mock('../../behavior-tree');
let root: BehaviorTree = null;
describe('Condition Node', () => {
  beforeEach(() => {
    root = new BehaviorTree({});
  });

  describe('Structure', () => {
    it('can be initilized', () => {
      const condition = new Condition({ predicate: null });

      expect(condition).toBeInstanceOf(Condition);
    });

    it('receives a predicate, upon pulsing evaluates the predicate', () => {
      const mockPredicate = jest.fn();
      const condition = new Condition({ predicate: mockPredicate });

      condition.pulse({ root }, BTStatus.Start);

      expect(mockPredicate).toBeCalled();
    });

    it('when predicate is truthy should have SUCCESS status', () => {
      const condition = new Condition({ predicate: () => true });
      condition.pulse({ root }, BTStatus.Start);

      expect(condition.status).toEqual(BTStatus.Success);
    });

    it('when predicate is falsy should have FAILURE status', () => {
      const condition = new Condition({ predicate: () => false });
      condition.pulse({ root }, BTStatus.Start);

      expect(condition.status).toEqual(BTStatus.Failure);
    });

    it('upon evaluating the predicate, should notify bt it\'s parent and itself in "node => parent => node" order', () => {
      const dummyParent = new Condition({ predicate: null });
      const condition = new Condition({ predicate: () => true });
      condition.setParent(dummyParent);
      const callOrder = [];
      jest.spyOn(root, 'notify').mockImplementation((node, status) => {
        callOrder.push(node);
      });

      condition.pulse({ root }, BTStatus.Start);
      
      expect(root.notify).toBeCalledWith(dummyParent, BTStatus.Success);
      expect(root.notify).toBeCalledWith(condition, BTStatus.Success);

      expect(callOrder[0]).toEqual(condition);
      expect(callOrder[1]).toEqual(dummyParent);
      expect(callOrder[2]).toEqual(condition);
    });
  });


  describe('State transitions', () => {
    it('is idle on initlization', () => {
      const condition = new Condition({ predicate: null });

      expect(condition.status).toEqual(BTStatus.Idle);
    });

    it('can only be pulsated with "START" status regardles of it\s current status', () => {
      const mockPredicate = jest.fn().mockImplementation(() => true);
      const condition = new Condition({ predicate: mockPredicate });

      condition.pulse({root},BTStatus.Idle);
      condition.pulse({root},BTStatus.Failure);
      condition.pulse({root},BTStatus.Running);
      condition.pulse({root},BTStatus.Success);
      expect(mockPredicate).not.toBeCalled();

      condition.pulse({root},BTStatus.Start);
      expect(mockPredicate).toBeCalled();
    });
  })

});