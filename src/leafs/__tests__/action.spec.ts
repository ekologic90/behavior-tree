import { BehaviorTree } from '../../behavior-tree';
import { Action } from '../action';
import { BTStatus } from '../../node';

jest.mock('../../behavior-tree');
let root: BehaviorTree = null;
describe('Action Node', () => {
  beforeEach(() => {
    root = new BehaviorTree({});
  });

  describe('Structure', () => {
    it('can be initilized', () => {
      const action = new Action({ action: null });
      expect(action).toBeInstanceOf(Action);
    });

    it('receives a callback upon pulsing calls it', () => {
      const mockCallback = jest.fn();
      const action = new Action({ action: mockCallback });
      action.pulse({ root }, BTStatus.Start);

      expect(mockCallback).toBeCalled();
    });

    it('notifies the bt, upon finishing it\'s action, action is performed synchronous', () => {
      const action = new Action({ action: () => { } });
      action.pulse({ root }, BTStatus.Start);

      expect(root.notify).toBeCalled();
    });

    it('upon finishing the action, should notify bt it\'s parent and itself in "parent => node" order', () => {
      const dummyParent = new Action({ action: () => { } });
      const action = new Action({ action: () => { } });
      action.setParent(dummyParent);
      const callOrder = [];
      jest.spyOn(root, 'notify').mockImplementation((node, status) => {
        callOrder.push(node);
      });

      action.pulse({ root }, BTStatus.Start);
      expect(root.notify).toBeCalledWith(dummyParent, BTStatus.Success);
      expect(root.notify).toBeCalledWith(action, BTStatus.Success);

      expect(callOrder[0]).toEqual(dummyParent);
      expect(callOrder[1]).toEqual(action);
    });

    it('notifies the bt asynchronous when "async" prop enabled, callback to notify the parent is passed', () => {
      const action = new Action({ action: (d) => d(), async: true });
      action.pulse({ root }, BTStatus.Start);

      expect(root.notify).toBeCalled();
    });
  })


  describe('State transitions', () => {
    it('is idle on initilization', () => {
      const action = new Action({ action: null });

      expect(action.status).toEqual(BTStatus.Idle);
    });

    it('when IDLE and on START transitions to RUNNING state and executes the action, upon completion transitions to SUCCESS', () => {
      const action = new Action({
        async: true, action: (done) => {
          expect(action.status).toEqual(BTStatus.Running);
          done();
          expect(action.status).toEqual(BTStatus.Success);
        }
      });

      action.pulse({ root }, BTStatus.Start);
    });

    it('when IDLE passed state should only update it\'s status without executing action', () => {
      const spy = jest.fn();
      const action = new Action({ action: () => spy() });
      action.status = BTStatus.Success;

      action.pulse({ root }, BTStatus.Idle);

      expect(action.status).toEqual(BTStatus.Idle);
      expect(spy).not.toBeCalled();
    });

    it('when SUCCESS and on START transitions to RUNNING state and executes the action, upon completion transitions to SUCCESS', () => {
      const action = new Action({
        async: true, action: (done) => {
          expect(action.status).toEqual(BTStatus.Running);
          done();
          expect(action.status).toEqual(BTStatus.Success);
        }
      });
      action.status = BTStatus.Success;
      action.pulse({ root }, BTStatus.Start);
    });

    it('when RUNNING should ignore any incoming pulses with different statuses', () => {
      const mockAction = jest.fn();
      const action = new Action({
        async: true,
        action: mockAction
      });

      action.status = BTStatus.Running;

      expect(action.status).toEqual(BTStatus.Running);
      action.pulse({ root }, BTStatus.Idle);
      expect(action.status).toEqual(BTStatus.Running);
      action.pulse({ root }, BTStatus.Failure);
      expect(action.status).toEqual(BTStatus.Running);
      action.pulse({ root }, BTStatus.Running);
      expect(action.status).toEqual(BTStatus.Running);
      action.pulse({ root }, BTStatus.Start);
      expect(action.status).toEqual(BTStatus.Running);
      action.pulse({ root }, BTStatus.Success);
      expect(action.status).toEqual(BTStatus.Running);
      expect(mockAction).not.toBeCalled();
    })
  });



});
