import { EventEmitter } from 'eventemitter3';
import { BehaviorTree } from '../../behavior-tree';
import { WaitForEvent } from '../wait-for-event';

jest.mock('../../behavior-tree');
let root: BehaviorTree = null;
describe('Condition Node', () => {
  beforeEach(() => {
    root = new BehaviorTree({});
  });

  describe('Structure', () => {
    it('can be initilized', () => {
      const waitForEvent = new WaitForEvent({ event: null });

      expect(waitForEvent).toBeInstanceOf(WaitForEvent);
    });
  });

  describe('State transitions', () => {

  });
});