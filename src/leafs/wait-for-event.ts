import { Leaf } from './leaf';
import { BTBoard, BTStatus } from '../node';

type WaitForEventProp = {
  event: string
}

export class WaitForEvent extends Leaf<WaitForEventProp>{
  private eventDisposer: () => void;
  public start(board: BTBoard): void {
    if (this.status == BTStatus.Running) {
      return;
    }
    const eventHandler = () => {
      this.status = BTStatus.Success;
      board.root.notify(this.parent, this.status);
      board.root.notify(this, this.status);
      if (this.eventDisposer) {
        this.eventDisposer();
      }
    }

    board.root.dispatcher.on(this.props.event, eventHandler);
    this.eventDisposer = () => board.root.dispatcher.off(this.props.event, eventHandler);
    this.status = BTStatus.Running;
    board.root.notify(this, this.status);
  }

  public cancel(board: BTBoard) {
    if (this.eventDisposer) {
      this.eventDisposer();
    }
  }
}