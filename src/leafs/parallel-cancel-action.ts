import { BTBoard, BTStatus } from "../node";
import { Leaf } from "./leaf";
import { Parallel } from '../composites/parallel';

type ParallelCancelActionProp = {
}
export class ParallelCancelAction extends Leaf<ParallelCancelActionProp> {
    public pulse(board: BTBoard, status: BTStatus): void {
        this.status = BTStatus.Running;
        let parent = this._parent;
        while (parent != null && !(parent instanceof Parallel)) {
            parent = parent.parent;
        }

        if (parent == null) { // TODO in prod this should not happen

        } else {
            this.status = BTStatus.Success;
            parent.cancel(board);
        }
    }
}
