import { Leaf } from './leaf';
import { BTBoard, BTStatus } from '../node';

type ConditionProp = {
  predicate: () => boolean
}

export class Condition extends Leaf<ConditionProp>{
  public start(board: BTBoard): void {
    this.status = BTStatus.Running;
    board.root.notify(this, this.status);

    this.status = BTStatus.Failure;
    if (this.props.predicate()){
      this.status = BTStatus.Success;
    } 
    board.root.notify(this.parent, this.status);
    board.root.notify(this, this.status);    
  }
}