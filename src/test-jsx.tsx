import { BehaviorTree } from './behavior-tree';
import { Sequencer } from './composites/sequencer';
import { Action } from './leafs/action';
import { Parallel } from './composites/parallel';
import { ParallelCancelAction } from './leafs/parallel-cancel-action';
import { Repeater } from './decorators/repeater'
import { ConditionalAction } from './custom-node/conditional-action';
import cytoscape from 'cytoscape';
import { WaitForEvent } from './leafs/wait-for-event';
import { RepeatUntil } from './decorators/repeat-until';
import { Condition } from './leafs/condition';
import { BTStatus } from './node';

const isFunction = function (o) { return typeof o === 'function'; };
let defaults = {
  // dagre algo options, uses default value on undefined
  nodeSep: undefined, // the separation between adjacent nodes in the same rank
  edgeSep: undefined, // the separation between adjacent edges in the same rank
  rankSep: undefined, // the separation between adjacent nodes in the same rank
  rankDir: undefined, // 'TB' for top to bottom flow, 'LR' for left to right,
  ranker: undefined, // Type of algorithm to assigns a rank to each node in the input graph.
  // Possible values: network-simplex, tight-tree or longest-path
  minLen: function (edge) { return 1; }, // number of ranks to keep between the source and target of the edge
  edgeWeight: function (edge) { return 1; }, // higher weight edges are generally made shorter and straighter than lower weight edges

  // general layout options
  fit: true, // whether to fit to viewport
  padding: 30, // fit padding
  spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
  nodeDimensionsIncludeLabels: false, // whether labels should be included in determining the space used by a node
  animate: false, // whether to transition the node positions
  animateFilter: function (node, i) { return true; }, // whether to animate specific nodes when animation is on; non-animated nodes immediately go to their final positions
  animationDuration: 500, // duration of animation in ms if enabled
  animationEasing: undefined, // easing of animation if enabled
  boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
  transform: function (node, pos) { return pos; }, // a function that applies a transform to the final node position
  ready: function () { }, // on layoutready
  stop: function () { } // on layoutstop
};
const dagre = require('dagre');

// constructor
// options : object containing layout options
function DagreLayout(options) {
  this.options = Object.assign({}, defaults, options);
}

// runs the layout
DagreLayout.prototype.run = function () {
  let options = this.options;
  let layout = this;

  let cy = options.cy; // cy is automatically populated for us in the constructor
  let eles = options.eles;

  let getVal = function (ele, val) {
    return isFunction(val) ? val.apply(ele, [ele]) : val;
  };

  let bb = options.boundingBox || { x1: 0, y1: 0, w: cy.width(), h: cy.height() };
  if (bb.x2 === undefined) { bb.x2 = bb.x1 + bb.w; }
  if (bb.w === undefined) { bb.w = bb.x2 - bb.x1; }
  if (bb.y2 === undefined) { bb.y2 = bb.y1 + bb.h; }
  if (bb.h === undefined) { bb.h = bb.y2 - bb.y1; }

  let g = new dagre.graphlib.Graph({
    multigraph: true,
    compound: true
  });

  let gObj = {};
  let setGObj = function (name, val) {
    if (val != null) {
      gObj[name] = val;
    }
  };

  setGObj('nodesep', options.nodeSep);
  setGObj('edgesep', options.edgeSep);
  setGObj('ranksep', options.rankSep);
  setGObj('rankdir', options.rankDir);
  setGObj('ranker', options.ranker);

  g.setGraph(gObj);

  g.setDefaultEdgeLabel(function () { return {}; });
  g.setDefaultNodeLabel(function () { return {}; });

  // add nodes to dagre
  let nodes = eles.nodes();
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i];
    let nbb = node.layoutDimensions(options);

    g.setNode(node.id(), {
      width: nbb.w * 2,
      height: nbb.h * 2,
      name: node.id()
    });

    // console.log( g.node(node.id()) );
  }

  // set compound parents
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i];

    if (node.isChild()) {
      g.setParent(node.id(), node.parent().id());
    }
  }

  // add edges to dagre
  let edges = eles.edges().stdFilter(function (edge) {
    return !edge.source().isParent() && !edge.target().isParent(); // dagre can't handle edges on compound nodes
  });
  for (let i = 0; i < edges.length; i++) {
    let edge = edges[i];

    g.setEdge(edge.source().id(), edge.target().id(), {
      minlen: getVal(edge, options.minLen),
      weight: getVal(edge, options.edgeWeight),
      name: edge.id()
    }, edge.id());

    // console.log( g.edge(edge.source().id(), edge.target().id(), edge.id()) );
  }

  dagre.layout(g);

  let gNodeIds = g.nodes();
  for (let i = 0; i < gNodeIds.length; i++) {
    let id = gNodeIds[i];
    let n = g.node(id);

    cy.getElementById(id).scratch().dagre = n;
  }

  let dagreBB;

  if (options.boundingBox) {
    dagreBB = { x1: Infinity, x2: -Infinity, y1: Infinity, y2: -Infinity };
    nodes.forEach(function (node) {
      let dModel = node.scratch().dagre;
      // Direct Acyclic Graphe
      dagreBB.x1 = Math.min(dagreBB.x1, dModel.x);
      dagreBB.x2 = Math.max(dagreBB.x2, dModel.x);

      dagreBB.y1 = Math.min(dagreBB.y1, dModel.y);
      dagreBB.y2 = Math.max(dagreBB.y2, dModel.y);
    });

    dagreBB.w = dagreBB.x2 - dagreBB.x1;
    dagreBB.h = dagreBB.y2 - dagreBB.y1;
  } else {
    dagreBB = bb;
  }

  let constrainPos = function (p) {
    if (options.boundingBox) {
      let xPct = dagreBB.w === 0 ? 0 : (p.x - dagreBB.x1) / dagreBB.w;
      let yPct = dagreBB.h === 0 ? 0 : (p.y - dagreBB.y1) / dagreBB.h;

      return {
        x: bb.x1 + xPct * bb.w,
        y: bb.y1 + yPct * bb.h,
      };
    } else {
      return p;
    }
  };

  nodes.layoutPositions(layout, options, function (ele) {
    ele = typeof ele === "object" ? ele : this;
    let dModel = ele.scratch().dagre;

    return constrainPos({
      x: dModel.x,
      y: dModel.y
    });
  });

  return this; // chaining
};


const helppage = null;

const t: BehaviorTree = (<BehaviorTree>
  <Sequencer>
    <Action async={true} action={(d) => { console.log('yeaahaah 1'); setTimeout(d, 500) }}></Action>
      <Action async={true} action={(d) => { console.log('yeaahaah 2'); setTimeout(d, 500) }}></Action>
      <Action async={true} action={(d) => { console.log('yeaahaah 3'); setTimeout(d, 500) }}></Action>
      <Action async={true} action={(d) => { console.log('yeaahaah 4'); setTimeout(d, 500) }}></Action>
      <Action async={true} action={(d) => { console.log('yeaahaah 5'); setTimeout(d, 500) }}></Action>
      <ConditionalAction predicate={() => true} action={() => console.log('CONDITIONAL ACTION')} />
      <Action async={true} action={(d) => { console.log('yeaahaah 6'); setTimeout(d, 500) }}></Action>
      <Action async={true} action={(d) => { console.log('yeaahaah 7'); setTimeout(d, 500) }}></Action>
      <Action async={true} action={(d) => { console.log('yeaahaah 8'); setTimeout(d, 500) }}></Action>
      <ConditionalAction predicate={() => false} action={() => console.log('CONDITIONAL ACTION')} />
      <ConditionalAction predicate={() => true} action={(d) => {console.log('CONDITIONAL ASYNC ACTION'); setTimeout(d,50)}} />
      <Action async={true} action={(d) => { console.log('yeaahaah 9'); setTimeout(d, 500) }}></Action>
      <Action async={true} action={(d) => { console.log('yeaahaah 10'); setTimeout(d, 500) }}></Action>
  </Sequencer>
</BehaviorTree>);
// const Wait = <Action async={true} action={()=>{}}/>

const GameFlowTree: any = {};
const board = {
  currentPage: 0,
  totalPage: 10
}
const view: any = null;

const helpPageTree: BehaviorTree = (
  <BehaviorTree {...board}>
    <RepeatUntil predicate={() => false}>
      <Sequencer>
        {/* <WaitForEvent event='help-page:open' coni/> */}

        <Action action={() => view.show(helpPageTree.board['currentPage'])} />
        <Parallel>
          <Sequencer>
            <WaitForEvent event='help-page:next' />
            <Action action={null} />
            <Action action={null} />
          </Sequencer>
          <Sequencer>
            <WaitForEvent event='help-page:prev' />
            <Action action={null} />
            <Action action={null} />
          </Sequencer>
          <Sequencer>
            <WaitForEvent event='help-page:goto' />
            <Action action={null} />
            <Action action={null} />
          </Sequencer>
          <Sequencer>
            <WaitForEvent event='help-page:close' />
            <ParallelCancelAction />
          </Sequencer>
        </Parallel>
        <Action action={null} />
      </Sequencer>
    </RepeatUntil>
  </BehaviorTree>
);

const SetCanOpenHP = <Action action={() => helpPageTree.board['canOpen'] = true} />
const SetCannotOpenHP = <Action action={() => helpPageTree.board['canOpen'] = false} />

const CanOpenHP = <Condition predicate={() => GameFlowTree.bord.isPlayingFiveOfAKind} />


//@ts-ignore
window['t'] = t;

t.start();

const div = document.createElement('div')
div.setAttribute('id', 'cy');
div.style.width = '100vw';
div.style.height = '100vh'
document.body.appendChild(div);

function getAllNodes(root) {
  const result = [];
  const stack = [root];
  while (stack.length > 0) {
    const node = stack.pop();
    result.push(node);
    stack.push(...(node.children || []));
  }
  return result;
}

function makeEdges(root) {
  const result = [];
  const stack = [root];
  let id = 0;
  while (stack.length > 0) {
    const node = stack.pop();
    const children = (node.children || []);
    result.push(...children.map(q => ({
      data: {
        id: `e${id++}`,
        source: node.id,
        target: q.id
      }
    })));
    stack.push(...(node.children || []));
  }
  return result;

}

const nodes = getAllNodes(t);
const edges = makeEdges(t);

// registers the extension on a cytoscape lib ref
let register = function (cytoscape) {
  if (!cytoscape) { return; } // can't register if cytoscape unspecified

  cytoscape('layout', 'dagre', DagreLayout); // register with cytoscape.js
};

if (typeof cytoscape !== 'undefined') { // expose to global cytoscape (i.e. window.cytoscape)
  register(cytoscape);
}
//@ts-ignore
const cy = cytoscape({

  container: document.getElementById('cy'),

  elements: [
    ...nodes.map((q, i): cytoscape.ElementDefinition => ({
      grabbable: false,
      data: {
        id: q.id,
        name: q.constructor.name
      }
    })),
    ...edges
  ],
  layout: {
    name: 'dagre'
  },
  // so we can see the ids
  style: [
    {
      selector: 'node',
      style: {
        'background-color': '#11479e',
        'label': 'data(name)',
        'font-size': '10px',
        'width': 25,
        'height': 25,
      }
    },
    {
      selector: 'edge',
      style: {
        'width': 2,
        'target-arrow-shape': 'triangle',
        'line-color': '#9dbaea',
        'target-arrow-color': '#9dbaea',
        'curve-style': 'bezier'
      }
    }
  ],
});
//export type BTStatus = 'IDLE' | 'START' | 'RUNNING' | 'SUCCESS' | 'FAILURE';

t.dispatcher.on('bt:pulse', (action) => {
  const { node } = action;
  const color = node.status == BTStatus.Start || node.status == BTStatus.Running ? '#42d3d3' :
    node.status == BTStatus.Success ? '#42d383' :
      node.status == BTStatus.Failure ? '#d34242' : '#11479e';
  cy.elements(`node#${node.id}`).style({
    'background-color': color
  })
})

