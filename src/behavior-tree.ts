import { Node, BTStatus, BTBoard } from "./node";
import { Composite } from "./composites/composite";
import { StackList } from "./linked-list";
import { Decorator } from "./decorators/decorator";
import { EventEmitter } from "events";

//@ts-ignore
const ticker = setImmediate || window.requestAnimationFrame;

type BehaviorTreeAction = {
  node: Node,
  status: BTStatus,
};
type BehaviorTreeProp = {

};

export class BehaviorTree extends Decorator<BehaviorTreeProp> {
  public get childrens() { return this.children; }
  private _isPulsing: boolean = false;
  private _stack: Array<BehaviorTreeAction> = []; // TODO switch to linked list?
  public board = {
    root: this
  }
  public dispatcher = new EventEmitter();

  public notify(node: Node, status: BTStatus) {
    this._stack.push({ node, status });
    if (!this._isPulsing) {
      this.pulse(this.board, BTStatus.Running);
    }
  }

  public pulse(board: BTBoard, status: BTStatus): void {
    ticker(() => {
      this._isPulsing = true;
      while (this._stack.length > 0) {
        const action = this._stack.pop();
        action.node.pulse(this.board, action.status);
        this.dispatcher.emit(`bt:pulse`, action);
      }
      this._isPulsing = false;
    });
  }


  public start() {
    this.board.root.notify(this.child, BTStatus.Start);
  }
  public stop() {
    
  }
  public pause() {
  }
  public resume() {
  }
  public restart() {
    this.stop();
    this.start();
  }

  public static createNode(type: new (prop) => Node, attr, ...children: Node[]) {

    const node = new type(attr);

    if (BehaviorTree.isContainer(node) && children.length > 0) {
      for (const c of children) {
        node.addChild(c);
        c.setParent(node);
      }
    }

    return node;
  }

  private static isContainer(obj: any): obj is Composite<any> {
    return obj.addChild != undefined;
  }
}
