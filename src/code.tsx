import { BehaviorTree } from "./behavior-tree";
import { Sequencer } from "./composites/sequencer";
import { Action } from "./leafs/action";

const bt: BehaviorTree = (<BehaviorTree>
    <Sequencer>
      <Action action={() => {console.log('yeaahaah')}}></Action>
      <Action action={() => {console.log('yeaahaah')}}></Action>
      <Action action={() => {console.log('yeaahaah')}}></Action>
      <Action action={() => {console.log('yeaahaah')}}></Action>
      <Action action={() => {console.log('yeaahaah')}}></Action>
      <Action action={() => {console.log('yeaahaah')}}></Action>
    </Sequencer>
  </BehaviorTree>);

bt.dispatcher.on('bt:pulse', (action) => {
    const { node } = action;
    console.log(action.node.id, action.node.status);
  })
bt.start();