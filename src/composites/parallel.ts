import { Composite } from "./composite";
import { BTBoard, BTStatus } from "../node";

type ParallelProps = {
    amountOfSuccess?: number,
    amountOfFailures?: number,
};

export class Parallel extends Composite<ParallelProps> {
    private amountOfSuccesses = 0;
    private amountOfFailures = 0;
    private get totalStatuses() { return this.amountOfFailures + this.amountOfSuccesses; }

    public pulse(board: BTBoard, status: BTStatus): void {
        if (status == BTStatus.Start) {
            this.amountOfFailures = 0;
            this.amountOfSuccesses = 0;
            this.status = BTStatus.Running;
            this.children.forEach((q) => {
                board.root.notify(q, BTStatus.Start)
            });
        }

        if (status == BTStatus.Failure) {
            this.amountOfFailures++;
        }
        else if (status == BTStatus.Success) {
            this.amountOfSuccesses++;
        }

        if (this.totalStatuses == this.children.length) {
            this.status = BTStatus.Success
            this.children
                .filter(q => q.status == BTStatus.Running)
                .forEach(q => q.cancel(board));
            board.root.notify(this._parent, this.status);
        }
    }

    public cancel(board) {
        // TODO REWORK CANCEL BEHAVIOR
        this.children
            .filter(q => q.status == BTStatus.Running)
            .forEach(q => q.cancel(board));
    }

}
