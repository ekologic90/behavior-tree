import { Composite } from "./composite";
import { BTBoard, BTStatus } from "../node";

type SequencerProps = {
  ignoreFails?: boolean;
}
export class Sequencer extends Composite<SequencerProps> {
  private pointer: number = 0;
  public pulse(board: BTBoard, status: BTStatus): void {
    if (status == BTStatus.Start) {
      this.pointer = 0;
      this.status = BTStatus.Running;
      this.children.forEach(q => board.root.notify(q, BTStatus.Idle));
    }

    if (status == BTStatus.Failure) {
      this.status = status;
      board.root.notify(this._parent, status);
      return;
    }

    if (this.pointer >= this.children.length) {
      this.status = status;
      board.root.notify(this._parent, BTStatus.Success);
      return;
    }

    const childToExecute = this.children[this.pointer++];
    board.root.notify(childToExecute, BTStatus.Start);
  }

  cancel() {
    this.pointer = this.children.length;
  }
}