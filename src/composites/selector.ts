import { Composite } from "./composite";
import { BTBoard, BTStatus } from "../node";

type SelectorProps = {

}
export class Selector extends Composite<SelectorProps> {
  private pointer: number = 0;

  public pulse(board: BTBoard, status: BTStatus): void {
    if (status == BTStatus.Start) {
      this.pointer = 0;
      this.status = BTStatus.Running;
    }

    if (status == BTStatus.Success) {
      this.status = status;
      board.root.notify(this._parent, status);
      return;
    }

    if (this.pointer >= this.children.length) {
      this.status = BTStatus.Failure;
      board.root.notify(this._parent, BTStatus.Failure)
      return;
    }

    const childToExecute = this.children[this.pointer++];
    board.root.notify(childToExecute, BTStatus.Start);
  }

  cancel() {
    this.pointer = this.children.length;
  }
}