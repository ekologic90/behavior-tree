import { Node } from "../node";

export abstract class Composite<Prop> extends Node<Prop> {
    protected children: Node[] = []; //  TODO should be linked list
    public addChild(node: Node) {
        this.children.push(node);
    }
}