


type LinkListNode<T> = {
  element: T,
  next?: LinkListNode<T>,
}

const NullNode: LinkListNode<any> = {
  element: 0,
}

// TODO convert to double ll
export class LinkedList<T> {
  private head: LinkListNode<T> = NullNode;

  insert(node: LinkListNode<T>) {
    node.next = NullNode;
    if (this.head == NullNode) {
      this.head = node;
    } else {
      node.next = this.head;
      this.head = node;
    }
  }

  remove(nodeToRemove: LinkListNode<T>) {
    if (this.head == NullNode || nodeToRemove == null) {
      return;
    }
    let pointer = this.head;
    while (pointer.next != null && pointer.next != nodeToRemove) {
      pointer = pointer.next;
    }

    if (pointer != null && pointer.next == nodeToRemove) {
      pointer.next = pointer.next.next;
    }
    if (this.head == nodeToRemove) {
      this.head = NullNode;
    }
  }

  peek() {
    return this.head;
  }
}


export class StackList<T> {
  private list = new LinkedList<T>()
  push(element: T) {
    this.list.insert({ element });
  }

  pop() {
    const peek = this.list.peek();
    this.list.remove(peek);
    return peek.element;
  }

  isEmpty() {
    return this.list.peek() == NullNode;
  }

  peek() {
    return this.peek().element;
  }
}